#include "Header.h"

/**
 * \brief Lit un fichier d'instance pour remplir une structure t_probleme.
 * \param P R�f�rence vers la structure de probl�me � remplir.
 * \param nom Chemin du fichier � ouvrir.
 */
void lire_fichier_instance(t_probleme& P, string nom) {
	// Variables locales pour stocker les valeurs lues
	int dist = 0,
		qtt = 0,
		ind = 0,
		i, j, k;

	// Ouverture du fichier en mode lecture
	std::ifstream fichier_ouvert(nom.c_str());

	if (fichier_ouvert.is_open()) {
		// Lecture des informations relatives au probl�me
		fichier_ouvert >> P.nb_de_client; // Lecture du nombre de clients
		fichier_ouvert >> P.nb_vehicule; // Lecture du nombre de v�hicules disponibles
		fichier_ouvert >> P.capa_vehicule; // Lecture de la capacit� des v�hicules

		// Lecture des distances entre les clients et stockage dans la matrice distance
		for (i = 0; i < P.nb_de_client + 1; i++) {
			for (j = 0; j < P.nb_de_client + 1; j++) {
				fichier_ouvert >> dist; // Lecture de la distance entre deux clients
				P.distance[i][j] = dist; // Stockage de la distance dans la matrice distance
			}
		}

		// Lecture des quantit�s associ�es � chaque client
		for (k = 1; k < P.nb_de_client + 1; k++) {
			fichier_ouvert >> ind; // Lecture de l'indice du client
			fichier_ouvert >> qtt; // Lecture de la quantit� associ�e au client
			P.quantite[ind] = qtt; // Stockage de la quantit� associ�e au client dans le tableau quantite
		}
		fichier_ouvert.close(); // Fermeture du fichier apr�s lecture
	}
	else {
		std::cout << "Ouverture ratee" << std::endl; // Affichage d'un message d'erreur si l'ouverture du fichier �choue
	}
}



/**
 * \brief V�rifie si un �l�ment donn� se trouve dans un tableau et retourne son indice.
 * \param tab_client Tableau d'entiers dans lequel chercher l'�l�ment.
 * \param client El�ment � rechercher dans le tableau.
 * \param taille Taille du tableau.
 * \return L'indice de l'�l�ment s'il est trouv� dans le tableau, sinon retourne -1.
 */
int verif_tab_client(int tab_client[], int client, int taille) {
	// Initialisation des variables locales
	int ind = 1, // Indice de parcours du tableau
		result = -1; // R�sultat initialis� � -1 (non trouv�)

	// Boucle de recherche dans le tableau
	while ((ind < taille + 1) && (tab_client[ind] != client))
		ind++; // Incr�mentation de l'indice tant que l'�l�ment n'est pas trouv� ou que la fin du tableau n'est pas atteinte

	// V�rification si l'�l�ment est trouv� dans le tableau
	if (tab_client[ind] == client)
		result = ind; // Si trouv�, mettre � jour le r�sultat avec l'indice de l'�l�ment

	return result; // Renvoi de l'indice de l'�l�ment trouv� ou -1 s'il n'est pas pr�sent dans le tableau
}


/**
 * \brief Fonction r�alisant la m�thode du "plus proche voisin" pour g�n�rer une solution initiale au Probl�me de Routage de V�hicules (VRP).
 * \param P Une r�f�rence vers la structure de donn�es t_probleme contenant les d�tails du probl�me de routage de v�hicules.
 * \param S Une r�f�rence vers la structure de donn�es t_solution o� la solution g�n�r�e sera stock�e.
 */
void plus_proche_voisin(t_probleme& P, t_solution& S)
{
	int i, j, ind = 1,
		tab_client[nb_max_client],
		taille_tab_client = P.nb_de_client,
		ind_min_client, ind_client,
		ind_min_colonne;

	// Initialisation du tableau tab_client avec les indices des clients � visiter
	for (i = 1; i < P.nb_de_client + 1; i++)
	{
		tab_client[i] = i;
	}

	int ind_ligne = 0;
	S.TSP_solution[1] = 0; // Le premier �l�ment de la solution est le d�p�t (client 0)

	// Parcours des clients pour construire la solution
	for (i = 1; i < P.nb_de_client + 2; i++)
	{
		ind_min_colonne = tab_client[1];
		ind_min_client = tab_client[1];

		j = 1;
		while (j < taille_tab_client + 1)
		{
			// Si le client n'est pas d�j� visit�
			if (tab_client[j] != ind_ligne)
			{
				// Comparaison des distances pour trouver le client le plus proche
				if (P.distance[ind_ligne][ind_min_colonne] >= P.distance[ind_ligne][tab_client[j]])
				{
					// V�rification de la pr�sence du client dans le tableau tab_client
					ind_client = verif_tab_client(tab_client, tab_client[j], taille_tab_client);
					if (ind_client != -1)
					{
						ind_min_colonne = tab_client[j];
						ind_min_client = ind_client;
					}
				}
			}
			j++;
		}

		// Mise � jour du tableau tab_client et de la solution S
		tab_client[ind_min_client] = tab_client[taille_tab_client];
		taille_tab_client--;
		S.TSP_solution[i + 1] = ind_min_colonne;
		ind_ligne = ind_min_colonne; // Mise � jour du dernier client visit�
	}
	S.TSP_solution[P.nb_de_client + 2] = 0;
}


/**
 * \brief Affiche les �l�ments du vecteur stock� dans la structure t_solution.
 * \param S Structure de type t_solution contenant le vecteur � afficher.
 * \param taille Taille du vecteur � afficher.
 */
void affiche_vecteur(t_solution& S, int taille)
{
	int i;
	for (i = 1; i < taille + 2; i++)
		cout << S.TSP_solution[i] << " ";
	cout << 0 << endl;
}

/**
 * \brief Affiche les éléments de la tournée stockée dans la structure t_tour.
 * \param T Structure de type t_tour contenant la tournée à afficher.
 */
void affiche_tournee(t_tour& T)
{
    int i;
    cout << "Affichage Tournee :" << endl;
    for (i = 0; i < T.nb_client_tour + 1; i++)
        cout << T.clients[i] << " ";
    cout << 0 << endl;
}

/**
 * \brief Ins�re un �l�ment dans une liste tri�e.
 * \param liste Liste � laquelle l'�l�ment doit �tre ajout�.
 * \param element �l�ment de type t_couple � ins�rer dans la liste.
 */
void poser(t_liste& liste, t_couple element)
{
	liste.n++; // Incr�mente le nombre d'�l�ments dans la liste

	if (liste.n > taille_liste + 1)
	{
		liste.n = taille_liste + 1;
	}

	liste.T[liste.n] = element; // Ajoute l'�l�ment � la liste

	int i = liste.n;
	// Trie la liste en fonction de la distance de chaque �l�ment
	while ((i > 1) && (liste.T[i].distance < liste.T[i - 1].distance))
	{
		// Effectue un �change entre les �l�ments pour maintenir la liste tri�e
		t_couple sauv = liste.T[i - 1];
		liste.T[i - 1] = liste.T[i];
		liste.T[i] = sauv;
		i = i - 1;
	}
}


/**
 * \brief Cherche l'indice d'un �l�ment dans un tableau.
 * \param tab_client Tableau d'entiers dans lequel chercher l'�l�ment.
 * \param taille Taille du tableau.
 * \param client �l�ment � rechercher dans le tableau.
 * \return L'indice de l'�l�ment s'il est trouv� dans le tableau, sinon retourne la taille du tableau.
 */
int cherche_idx(int tab_client[], int taille, int client)
{
	int idx = 0; // Initialise l'indice � z�ro
	// Parcourt le tableau jusqu'� trouver l'�l�ment ou atteindre la fin du tableau
	while ((tab_client[idx] != client) && (idx < taille))
	{
		idx++; // Incr�mente l'indice pour passer � l'�l�ment suivant
	}
	return idx; // Retourne l'indice de l'�l�ment s'il est trouv�, sinon retourne la taille du tableau
}


/**
 * \brief Utilise une variante al�atoire de l'algorithme du "plus proche voisin" pour g�n�rer une solution initiale.
 * \param P R�f�rence vers la structure de donn�es t_probleme contenant les d�tails du probl�me de routage de v�hicules.
 * \param S R�f�rence vers la structure de donn�es t_solution o� la solution g�n�r�e sera stock�e.
 */
void plus_proche_voisin_randomise(t_probleme& P, t_solution& S)
{
	int i, j, ind = 0,
		tab_client[nb_max_client],
		depart,
		nb_client_pas_classe,
		voisin,
		distance,
		idx;

	srand((unsigned)time(0)); // Initialisation du g�n�rateur de nombres al�atoires

	double random,
		proba = 0.8; // Probabilit� de choix du voisin suivant

	bool continu;

	// Initialisation du tableau des clients � visiter
	for (i = 1; i < P.nb_de_client + 1; i++)
	{
		tab_client[i] = i;
	}
	nb_client_pas_classe = P.nb_de_client;

	S.TSP_solution[1] = 0; // Le premier �l�ment de la solution est le d�p�t

	for (i = 1; i <= P.nb_de_client; i++)
	{
		t_liste liste;
		liste.n = 0;
		depart = S.TSP_solution[i];

		// Cr�ation d'une liste des voisins non visit�s et tri�s par distance
		for (j = 1; j <= nb_client_pas_classe; j++)
		{
			voisin = tab_client[j];
			distance = P.distance[depart][voisin];
			t_couple element;
			element.client = voisin;
			element.distance = distance;
			poser(liste, element);
		}

		continu = true;
		ind = 0;

		// Choix al�atoire du prochain voisin en fonction de la probabilit�
		while (continu)
		{
			ind++;
			random = ((double)rand() / (RAND_MAX));
			if ((random <= proba) || (ind >= liste.n))
			{
				S.TSP_solution[i + 1] = liste.T[ind].client;
				continu = false;
			}
		}

		// Mise � jour du tableau des clients � visiter
		idx = cherche_idx(tab_client, nb_client_pas_classe, liste.T[ind].client);
		tab_client[idx] = tab_client[nb_client_pas_classe];
		nb_client_pas_classe--;
	}

	// Le dernier �l�ment de la solution est le d�p�t pour boucler le circuit
	S.TSP_solution[P.nb_de_client + 2] = 0;
}

void decalage_solution_clarke(t_probleme& P, t_solution& S) {
	for (int i = 0; i <= P.nb_de_client + 1; i++) {
		S.TSP_solution[i + 1] = S.TSP_solution[i];
	}
}
/**
 * \brief Implemente l'algorithme Clarke-Wright pour construire une solution initiale au probl�me de routage de v�hicules.
 * \param P Reference vers la structure de donn�es t_probleme contenant les d�tails du probl�me de routage de v�hicules.
 * \param S Reference vers la structure de donn�es t_solution o� la solution g�n�r�e sera stock�e.
 */
void clarke_wright(t_probleme& P, t_solution& S) {
	// Initialisation du vecteur solution S
	S.TSP_solution[0] = 0; // D�p�t comme premier point
	S.TSP_solution[1] = -1; // Marqueur de fin de liste

	// Initialisation des variables locales
	int nb_clients_non_visites = P.nb_de_client;
	std::vector<int> tab_clients_non_visites(nb_clients_non_visites + 1);
	// Remplissage du tableau des clients non visit�s
	for (int i = 0; i <= nb_clients_non_visites; ++i) {
		tab_clients_non_visites[i] = i;
	}

	// Calcul des �conomies r�alis�es en reliant chaque paire de clients au d�p�t
	std::vector<std::vector<int>> economies(nb_clients_non_visites + 1, std::vector<int>(nb_clients_non_visites + 1));	
	for (int i = 0; i <= nb_clients_non_visites; ++i) {
		for (int j = i + 1; j < nb_clients_non_visites; ++j) {
			economies[i][j] = P.distance[0][tab_clients_non_visites[i]] + P.distance[0][tab_clients_non_visites[j]] - P.distance[tab_clients_non_visites[i]][tab_clients_non_visites[j]];
		}
	}

	// Tri des �conomies dans l'ordre d�croissant
	struct economy_sort {
		int i;
		int j;
		int saving;
	};
	int index = 0;
	std::vector<economy_sort> economies_triees(nb_max_client * nb_max_client);

	// Remplissage de la structure �conomie tri�e
	for (int i = 0; i <= nb_clients_non_visites; ++i) {
		for (int j = i + 1; j <= nb_clients_non_visites; ++j) {
			economies_triees[index].i = tab_clients_non_visites[i];
			economies_triees[index].j = tab_clients_non_visites[j];
			economies_triees[index].saving = economies[i][j];
			index++;
		}
	}

	// Tri des economies
	std::sort(economies_triees.begin(), economies_triees.begin() + index, [](const economy_sort& a, const economy_sort& b) {
		return a.saving > b.saving;
		});

	// Construction du chemin en utilisant les �conomies les plus importantes
	int position_insertion = 2; // Position d'insertion dans le vecteur S
	int k;

	// Parcours des �conomies tri�es
	for (k = 0; k < index && position_insertion < P.nb_de_client + 2; ++k) {
		int client1 = tab_clients_non_visites[economies_triees[k].i];
		int client2 = tab_clients_non_visites[economies_triees[k].j];

		// V�rification si les clients ne sont pas d�j� dans la tourn�e
		bool client1_in_tour = false, client2_in_tour = false;
		for (int j = 1; S.TSP_solution[j] != -1; j += 1) {
			if (S.TSP_solution[j - 1] == client1 || S.TSP_solution[j - 1] == client2) {
				client1_in_tour = true;
			}
			if (S.TSP_solution[j] == client1 || S.TSP_solution[j] == client2) {
				client2_in_tour = true;
			}
		}

		// Ajout des clients dans le chemin s'ils ne sont pas d�j� dans la tourn�e
		if (!client1_in_tour && !client2_in_tour) {
			S.TSP_solution[position_insertion] = client1;
			S.TSP_solution[position_insertion + 1] = client2;
			S.TSP_solution[position_insertion + 2] = -1; // Marqueur de fin de liste
			position_insertion += 2; // Avancement de trois positions pour la prochaine insertion
		}
	}

	// Ajout du dernier �l�ment s'il y a un nombre impair d'�conomies et qu'on a fini de parcourir toutes les �conomies
	if (index % 2 != 0 && k == index) {
		bool elements_present[nb_max_client] = { false }; // Initialisation d'un tableau pour marquer les �l�ments pr�sents

		// Marquage des �l�ments pr�sents dans le vecteur
		for (int i = 1; i <= P.nb_de_client; ++i) {
			if (S.TSP_solution[i] >= 0 && S.TSP_solution[i] <= P.nb_de_client) {
				elements_present[S.TSP_solution[i]] = true;
			}
		}

		// Recherche de l'�l�ment manquant
		for (int i = 1; i <= P.nb_de_client; ++i) {
			if (!elements_present[i]) {
				S.TSP_solution[position_insertion] = i;
			}
		}
	}

	S.TSP_solution[P.nb_de_client + 1] = 0; // Fermeture du circuit avec le d�p�t

	decalage_solution_clarke(P, S);

}


/**
 * \brief Calcule le coût total du problème du voyageur de commerce pour une solution donnée.
 * \param P Structure de type t_probleme contenant les données du problème.
 * \param S Structure de type t_solution contenant la solution à évaluer.
 * \return Le coût total de la solution pour le problème du voyageur de commerce.
 */
int calcul_cout_TSP(t_probleme& P, t_solution& S)
{
    int i;
    int cout = 0;

    // Parcours des clients dans l'ordre de la solution
    for (i = 1; i < P.nb_de_client + 1; i++)
    {
        cout += P.distance[S.TSP_solution[i]][S.TSP_solution[i + 1]];
    }

    // Ajout du coût pour revenir au premier client
    cout += P.distance[S.TSP_solution[i + 1]][S.TSP_solution[1]];

    return cout;
}


/**
 * \brief Calcule le co�t total d'un tour pour le Probl�me de Routage de V�hicules (VRP).
 * \param P R�f�rence vers la structure de donn�es t_probleme contenant les d�tails du probl�me.
 * \param T R�f�rence vers la structure de donn�es t_tour contenant les informations du tour.
 */
void calcul_cout_tour(t_probleme& P, t_tour& T)
{
	int i = 0;
	T.cout = P.distance[0][T.clients[1]];
	if (T.nb_client_tour - 1 > 1)
	{
		for (i = 1; i < T.nb_client_tour - 1; i++)
		{
			T.cout += P.distance[T.clients[i]][T.clients[i + 1]];
		}
	}
	T.cout += P.distance[T.clients[i + 1]][0];
}

/**
 * \brief Calcule le coût total de la solution en additionnant les coûts des différentes tournées.
 * \param S Structure de type t_solution contenant la solution à évaluer.
 */
void calcul_cout(t_solution& S)
{
    S.total_cout = 0;
    int i;

    // Parcours des tournées dans la solution
    for (i = 1; i < S.nb_tournee; i++)
    {
        S.total_cout += S.itineraire[i].cout;
    }
}



/**
 * \brief Applique l'algorithme 2-opt pour effectuer une recherche locale sur une tourn�e dans le Probl�me de Routage de V�hicules (VRP).
 * \param P R�f�rence vers la structure de donn�es t_probleme contenant les d�tails du probl�me.
 * \param T R�f�rence vers la structure de donn�es t_tour contenant les informations de la tourn�e � am�liorer.
 */
void deux_opt_une_tournee(t_probleme& P, t_tour& T)
{
	int j, 
		delta,
		temp,
		ind_deb,
		ind_fin,
		i = 1;
	bool continu;

	while (i <= T.nb_client_tour - 2) {
		j = i + 2;
		continu = true;
		while (j <= T.nb_client_tour && continu) {
			// Calcul du delta bas� sur la modification potentielle dans le co�t du parcours
			delta = P.distance[T.clients[i]][T.clients[j]] + P.distance[T.clients[i + 1]][T.clients[j + 1]] - P.distance[T.clients[i]][T.clients[i + 1]] - P.distance[T.clients[j]][T.clients[j + 1]];

			// D�tection d'une am�lioration potentielle du parcours
			j++;
			if (delta < 0)
				continu = false;
		}

		// Si une am�lioration potentielle a �t� d�tect�e
		if (delta < 0) {
			T.cout += delta; // Mise � jour du co�t total de la tourn�e avec le delta

			// Inversion de la s�quence entre i et j+1 non inclus
			i++;
			j--;
			while (i < j) {
				temp = T.clients[j];
				T.clients[j] = T.clients[i];
				T.clients[i] = temp;
				i++;
				j--;
			}
			i = 1; // Recommencer depuis le debut avec le nouveau vecteur
		}
		else
			i++; // Avancer dans le vecteur si aucune amelioration n'est detectee
	}
}




/**
 * \brief Applique une recherche locale avec insertion d'un sommet dans une tournee pour ameliorer la solution du Probleme de Routage de Vehicules (VRP).
 * \param P Reference vers la structure de donnees t_probleme contenant les details du probleme.
 * \param T Reference vers la structure de donnees t_tour contenant les informations de la tournee a ameliorer.
 */
void deplacement_sommet(t_probleme& P, t_tour& T)
{
	int delta_plus,
		delta_moins,
		j, 
		temp, 
		i = 0; // on demarre au depot du tour T
	bool continu = true;

	while (i < T.nb_client_tour)
	{
		// Calcul des deltas pour les insertions potentielles
		delta_plus = infini;
		delta_moins = -P.distance[T.clients[i]][T.clients[i + 1]] - P.distance[T.clients[i + 1]][T.clients[i + 2]] + P.distance[T.clients[i]][T.clients[i + 2]];

		// Recherche de l'emplacement optimal pour l'insertion du sommet
		j = 0;
		continu = true;

		while (j <= T.nb_client_tour && continu) {
			if ((j != (i + 1)) && (j != i)) {
				delta_plus = -P.distance[T.clients[j]][T.clients[j + 1]] + P.distance[T.clients[j]][T.clients[i + 1]] + P.distance[T.clients[i + 1]][T.clients[j + 1]];
			}

			// Test si l'insertion du sommet am�liore le co�t total de la tourn�e
			if (delta_moins + delta_plus < 0)
				continu = false;
			else {
				j++;
			}
		}

		// Si l'insertion am�liore le co�t de la tourn�e
		if (delta_moins + delta_plus < 0)
		{
			T.cout = T.cout + delta_moins + delta_plus;

			// D�placement du sommet i+1 entre j et j+1
			temp = T.clients[i + 1];

			if ((i + 1) < j) {
				for (int k = i + 1; k < j; k++) {
					T.clients[k] = T.clients[k + 1];
				}
				T.clients[j] = temp;
			}
			else {
				for (int k = i + 1; k > j; --k) {
					T.clients[k] = T.clients[k - 1];
				}
				T.clients[j + 1] = temp;
			}

			i = 0; // Reprise depuis le debut avec le nouveau vecteur
		}
		else
			i++; // Avancer dans la tournee si aucune amelioration n'est detectee
	}
}



/**
 * \brief Applique une recherche locale avec insertion inter-tournee pour ameliorer deux solutions du Probleme de Routage de Vehicules (VRP).
 * \param P Reference vers la structure de donnees t_probleme contenant les details du probleme.
 * \param T1 Reference vers la premiere structure de donnees t_tour representant une tournee a ameliorer.
 * \param T2 Reference vers la seconde structure de donnees t_tour representant une autre tournee a ameliorer.
 */
void insert_inter_tournee(t_probleme& P, t_tour& T1, t_tour& T2) {
	int delta_moins, 
		i, 
		delta_plus = infini, 
		cout, sommet, 
		ind, 
		resultat = 1, 
		j = 0; // Commencer au d�p�t

	int cout_min = infini, j_min;

	verif_tour(T1);
	verif_tour(T2);

	// Parcours de la premi�re tourn�e (T1) pour tester chaque sommet un par un
	for (i = 0; i < T1.nb_client_tour; i++)
	{
		verif_tour(T1);
		verif_tour(T2);
		// Calcul du delta moins pour l'insertion du sommet
		delta_moins = -P.distance[T1.clients[i]][T1.clients[i + 1]] - P.distance[T1.clients[i + 1]][T1.clients[i + 2]] + P.distance[T1.clients[i]][T1.clients[i + 2]];
		sommet = T1.clients[i + 1];

		// Test d'insertion du sommet de T1 dans T2 � diff�rents endroits
		for (j = 0; j <= T2.nb_client_tour; j++) {
 			delta_plus = -P.distance[T2.clients[j]][T2.clients[j + 1]] + P.distance[T2.clients[j]][sommet] + P.distance[sommet][T2.clients[j + 1]];
			cout = delta_moins + delta_plus;

			// Si une insertion am�liore le co�t total et le co�t est inf�rieur au co�t minimal actuel
			if ((cout < 0) && (cout < cout_min)) {
				cout_min = cout;
				j_min = j;
			}
		}

		// Si un emplacement d'insertion am�liore le co�t total de la tourn�e T2
		if (cout_min != infini)
		{
			// Mise � jour des co�ts des tours T1 et T2
			T2.cout = T2.cout + delta_plus;
			T1.cout = T1.cout + delta_moins;

			// Insertion du sommet dans T2 � l'emplacement optimal trouv�
			for (ind = T2.nb_client_tour + 1; ind > j_min; --ind) {
				T2.clients[ind] = T2.clients[ind - 1];
			}
			T2.clients[j_min + 1] = sommet;
			T2.nb_client_tour++;

			// Suppression du sommet de T1
			for (ind = i + 1; ind < T1.nb_client_tour; ind++) {
				T1.clients[ind] = T1.clients[ind + 1];
			}
			T1.nb_client_tour--;
		}
	}
}

void verif_tour(t_tour& T) {
	T.clients[0] = 0;
	T.clients[T.nb_client_tour + 1] = 0;
}


/**
 * \brief Applique l'optimisation 2-OPT inter-tournee pour ameliorer deux solutions du Probleme de Routage de Vehicules (VRP).
 * \param P Reference vers la structure de donnees t_probleme contenant les details du probleme.
 * \param T1 Reference vers la premiere structure de donnees t_tour representant une tournee a ameliorer.
 * \param T2 Reference vers la seconde structure de donnees t_tour representant une autre tournee a ameliorer.
 */
void deux_opt_inter_tournee(t_probleme& P, t_tour& T1, t_tour& T2) {
	verif_tour(T1);
	verif_tour(T2);
	int j, i, t, delta, temp, j_temp, i_temp;
	int index_t2;
	bool continu;

	int delta_min, j_min;

	t_tour tab_temp;
	i = 1;
	// Parcours de T1 pour évaluer chaque arc
	while (i < T1.nb_client_tour)
	{
		if ((T1.nb_client_tour >=2) && (T2.nb_client_tour >= 2)) {
			verif_tour(T1);
			verif_tour(T2);
			delta_min = infini;
			// Pour chaque arc, recherche du meilleur point d'insertion dans T2
			for (j = 0; j <= T2.nb_client_tour; j++) {
				delta = P.distance[T1.clients[i]][T2.clients[j + 1]] + P.distance[T1.clients[i + 1]][T2.clients[j]] - P.distance[T1.clients[i]][T1.clients[i + 1]] - P.distance[T2.clients[j]][T2.clients[j + 1]];
				if ((delta < 0) && (delta < delta_min)) {
					delta_min = delta;
					j_min = j;
				}
			}
			j--;
			// Si un meilleur point d'insertion a été trouvé dans T2
			if (delta_min != infini)
			{
				j_temp = j_min;
				i_temp = i;

				// Copie des valeurs de T1 dans un tableau temporaire
				tab_temp = T1;

				// Réorganisation des tournées T1 et T2
				while (j_min != T2.nb_client_tour + 1) {
					T1.clients[i + 1] = T2.clients[j_min + 1];
					i++;
					j_min++;
				}
				T1.nb_client_tour = i - 1;

				j_min = j_temp;
				i = i_temp;
				while (i != tab_temp.nb_client_tour) {
					T2.clients[j_min + 1] = tab_temp.clients[i + 1];
					i++;
					j_min++;
				}
				T2.nb_client_tour = j_min;

				i = 1; // Reprise depuis le début avec les nouveaux vecteurs
			}
			else
				i++;
		}
		else {
			i = infini;
		}
	}

	// Calcul du co�t pour les deux tourn�es
	calcul_cout_tour(P, T1);
	calcul_cout_tour(P, T2);


}

// Convertit la représentation en tours de la solution S en un vecteur TSP_solution
void tour_to_solution(t_probleme& P, t_solution& S) {
	int ind = 1; // Indice de la première position dans le vecteur TSP_solution

	// Initialisation du vecteur TSP_solution avec le dépôt
	S.TSP_solution[ind] = 0; // Le dépôt est toujours à l'indice 0 du vecteur
	ind++; // Passage à l'indice suivant

	// Parcours de tous les tours de la solution pour recréer le vecteur TSP_solution
	for (int i = 1; i < S.nb_tournee; i++) { // Parcourt tous les tours de la solution
		for (int j = 1; j < S.itineraire[i].nb_client_tour + 1; j++) { // Parcourt un tour spécifique
			// Ajoute chaque client du tour dans le vecteur TSP_solution
			S.TSP_solution[ind] = S.itineraire[i].clients[j];
			ind++; // Passage à la position suivante dans le vecteur
		}
	}
}



/**
 * \brief Détermine si le label P domine le label L.
 * \param P Label P à comparer.
 * \param L Label L à comparer.
 * \return 1 si P domine L, 2 si L domine P, 0 sinon.
 */
int domine(t_label P, t_label L) {
    int res = 0;
    if (P.cout <= L.cout && P.nb_vehicule >= L.nb_vehicule)
        res = 1;
    else if (P.cout >= L.cout && P.nb_vehicule <= L.nb_vehicule)
        res = 2;
    return res;
}

/**
 * \brief Ajoute le label l à la liste de labels, éliminant ceux qui sont dominés.
 * \param liste Liste de labels à mettre à jour.
 * \param l Label à ajouter.
 * \return 1 si le label a été ajouté, 0 sinon.
 */
int ajoute_label(t_list_label liste, t_label l) {
    int i = 0;
    int res = -1;

    while (res != 1 && i < liste.nb_label)
    {
        res = domine(liste.label[i], l);

        if (res == 2) {
            liste.label[i] = liste.label[liste.nb_label];
            --liste.nb_label;
        }

        i++;
    }

    if (res != 1)
    {
        liste.label[liste.nb_label] = l;
        liste.nb_label++;
        return 1;
    }
    return 0;
}

/**
 * \brief Effectue la phase de Split-Véhicule-Fini pour la résolution du problème de routage de véhicules.
 * \param T Structure de type t_probleme contenant les données du problème.
 * \param S Structure de type t_solution contenant la solution à mettre à jour.
 */
void Split_Vehicule_Fini(t_probleme& T, t_solution& S) {
    int i, j, k, p, res, imin;
    int load;
    int dist;
    int coutmax;
    t_label Q;

    // Initialisation du premier label
    S.labels[0].nb_label = 1;
    S.labels[0].label[0].cout = 0;
    S.labels[0].label[0].sommet_pere = -1;
    S.labels[0].label[0].nb_vehicule = T.nb_vehicule;

    // Initialisation des autres labels
    for (i = 1; i <= T.nb_de_client + 1; i++) {
        S.labels[i].nb_label = 0;
    }

    // Parcours des sommets
    for (i = 1; i <= T.nb_de_client + 1; i++) {
        j = i + 1;
        load = 0;

        // Construction des labels pour chaque sommet
        while (j <= T.nb_de_client + 1 && load + T.quantite[S.TSP_solution[j]] <= T.capa_vehicule && S.labels[i].nb_label < T.nb_vehicule) {
            load += T.quantite[S.TSP_solution[j]];
            if (j == i + 1) {
                dist = T.distance[S.TSP_solution[j]][0] + T.distance[0][S.TSP_solution[j]];
            }
            else {
                dist = dist + (T.distance[S.TSP_solution[j]][0] + T.distance[S.TSP_solution[j - 1]][S.TSP_solution[j]] - T.distance[S.TSP_solution[j - 1]][0]);
            }

            // Traitement des labels
            for (k = 0; k < S.labels[i].nb_label; k++) {
                Q = S.labels[i].label[k];
                Q.nb_vehicule--;
                if (Q.nb_vehicule > 0) {
                    Q.cout = Q.cout + dist;
                    Q.sommet_pere = i;
                    Q.position_pere = k;
                    res = ajoute_label(S.labels[j], Q);

                    // Insertion du label si nécessaire
                    if (res == 0) {
                        coutmax = S.labels[j].label[0].cout;
                        imin = 0;
                        for (int s = 1; s < 50; s++) {
                            if (S.labels[j].label[s].cout > coutmax) {
                                coutmax = S.labels[j].label[s].cout;
                                imin = s;
                            }
                        }
                        if (Q.cout < coutmax) {
                            S.labels[j].label[imin] = Q;
                        }
                    }
                }
            }
            j++;
        }
    }
}



/**
 * \brief Effectue la phase de Split pour la résolution du problème de routage de véhicules.
 * \param P Structure de type t_probleme contenant les données du problème.
 * \param S Structure de type t_solution à mettre à jour avec la phase de Split.
 */
void split(t_probleme& P, t_solution& S)
{
    // Déclarations
    int i, j, z;
    int val, cout = 0;
    int Pere[nb_max_client];
    int date[nb_max_client];
    date[1] = 0;
    Pere[1] = 0;

    S.total_cout = 0;

    // Initialisation des tableaux
    for (z = 2; z < P.nb_de_client + 1; z++) {
        Pere[z] = 0;
        date[z] = infini;
    }

    // Calcul des dates de début pour chaque client
    for (i = 1; i < P.nb_de_client; i++) {
        j = i + 1;
        val = 0;
        do {
            if ((i + 1) == j)
            {
                cout = P.distance[0][S.TSP_solution[j]] + P.distance[S.TSP_solution[j]][0];
                val = P.quantite[S.TSP_solution[j]];
            }
            else
            {
                cout = cout - P.distance[S.TSP_solution[j - 1]][0] + P.distance[S.TSP_solution[j - 1]][S.TSP_solution[j]] + P.distance[S.TSP_solution[j]][0];
                val = val + P.quantite[S.TSP_solution[j]];
            }

            if ((date[i] + cout) < date[j])
            {
                date[j] = date[i] + cout;
                Pere[j] = i;
            }

            j++;

        } while (j <= P.nb_de_client && (val + P.quantite[S.TSP_solution[j]]) <= P.capa_vehicule);
    }

    int t, pt,
        tour = 0,
        tour_prec = 0,
        cout_max = 0,
        cout_total = 0;

    cout = 0;

    S.nb_tournee = 0;

    // Construction des tournées
    for (int i = 1; i < P.nb_de_client + 1; i++)
    {
        t = Pere[i];
        pt = S.TSP_solution[i];

        if (t != tour_prec)
        {
            if (tour != 0)
            {
                calcul_cout_tour(P, S.itineraire[tour]);
            }
            else
            {
                S.itineraire[tour].cout = cout;
            }

            tour++;
            S.itineraire[tour].nb_client_tour = 0;
            S.itineraire[tour].capacite[0] = 0;
            S.itineraire[tour].clients[0] = 0;

            cout_max = 0;
        }

        S.itineraire[tour].nb_client_tour++;

        S.itineraire[tour].clients[S.itineraire[tour].nb_client_tour] = pt;
        tour_prec = t;

        cout = date[i];
    }

    // Mise à jour des derniers clients
    for (int i = 1; i <= P.nb_de_client; i++)
        S.itineraire[i].clients[S.itineraire[i].nb_client_tour + 1] = 0; // On met le dépôt à la fin de la liste

    calcul_cout_tour(P, S.itineraire[tour]);
    S.nb_tournee = ++tour;
    calcul_cout(S);
}

/**
 * \brief Effectue la phase inverse de Split pour la résolution du problème de routage de véhicules.
 * \param P Structure de type t_probleme contenant les données du problème.
 * \param S Structure de type t_solution contenant la solution à inverser.
 * \return Une nouvelle solution inversée.
 */
t_solution split_inverse(t_probleme P, t_solution& S)
{
    t_solution nv_S;

    int k = 1;

    nv_S.TSP_solution[k] = 0; // Dépôt
    k++;

    // Construction de la nouvelle solution inversée
    for (int i = 1; i < S.nb_tournee; i++)
    {
        if (S.itineraire[i].nb_client_tour)
        {
            for (int j = 1; j <= S.itineraire[i].nb_client_tour; j++)
            {
                nv_S.TSP_solution[k] = S.itineraire[i].clients[j];
                k++;
            }
        }
    }

    return nv_S;
}


// Cette fonction effectue une recherche d'une solution pour le problème du Voyageur de Commerce (TSP).
// Elle utilise un algorithme basé sur des heuristiques et la recherche locale pour optimiser la solution.
void Recherche_TSP(t_probleme P, t_solution& S, int nb_iter) {
	// Initialise le moteur de nombres aléatoires
	std::mt19937 mt_engine(std::random_device{}());

	double proba;
	int choix,
		iter,
		cout;

	// Au hasard pour la première itération
	std::cout << "Premiere iteration (avec Randomise + SPLIT) :" << std::endl;
	plus_proche_voisin_randomise(P, S); // Utilise l'heuristique du plus proche voisin randomisé
	split(P, S); // Divise la solution en tournées
	affiche_vecteur(S, P.nb_de_client); // Affiche la solution
	calcul_cout(S); // Calcule le coût total de la solution
	std::cout << "Cout : " << S.total_cout << " km(s)" << endl;

	t_solution S1 = S; // Copie de la solution actuelle pour comparer les améliorations


	for (iter = 0; iter < nb_iter; iter++) {
		// Génère un nombre aléatoire pour choisir entre deux heuristiques
		std::uniform_real_distribution<double> distribution(0.0, 1.0);
		proba = distribution(mt_engine);
		choix = (proba < 0.5) ? 0 : 1; // Choix aléatoire entre deux heuristiques

		// Applique une des deux heuristiques en fonction du choix aléatoire
		switch (choix) {
		case 0:
			plus_proche_voisin(P, S1); // Utilise l'heuristique du plus proche voisin
			break;
		case 1:
			plus_proche_voisin_randomise(P, S1); // Utilise l'heuristique du plus proche voisin randomisé
			break;
		default:
			break;
		}

		Recherche_VRP(P, S1, nb_iter); // Applique l'algorithme de recherche locale VRP sur la solution obtenue

		// Vérifie si la nouvelle solution est meilleure et met à jour la solution si c'est le cas
		if (S1.total_cout < S.total_cout) {
			S = S1; // Met à jour la solution
			std::cout << "===============================================\n" << endl;
			std::cout << "Nouvelle solution trouvee apres les heuristiques et les fonctions d'optimisation de tour : \n" << endl;
			affiche_vecteur(S, P.nb_de_client); // Affiche la nouvelle solution
			std::cout << "Cout : " << S.total_cout << " km(s)" << endl;
		}
	}
	calcul_cout(S); // Recalcule le coût total de la solution finale
	std::cout << "/=========================================================================================\n" << endl;
	std::cout << "Solution optimale finale :" << endl;
	affiche_vecteur(S, P.nb_de_client); // Affiche la solution finale
	std::cout << "Cout optimale : " << S.total_cout << " km(s)" << endl;
	std::cout << "/=========================================================================================\n" << endl;
}




/**
 * \brief Effectue une recherche locale pour résoudre le Problème de Routage de Véhicules (VRP).
 * \param P La structure de données t_probleme contenant les détails du problème.
 * \param S La structure de données t_solution contenant la solution à améliorer.
 * \param nb_iter Le nombre d'itérations pour la recherche locale.
 */
void Recherche_VRP(t_probleme P, t_solution& S, int nb_iter)
{
	std::mt19937 mt_engine(std::random_device{}()); // Générateur de nombres aléatoires

	double proba;
	int choix,
		iter,
		tournee_index,
		tournee_index_1,
		tournee_index_2;

	t_solution S1 = S; // Copie de la solution initiale

	for (iter = 0; iter < nb_iter; iter++) {

		// Séparation des tournées existantes
		split(P, S1);

		// Calcul du coût de chaque tournée dans la solution
		for (int i = 1; i < S1.nb_tournee; i++)
		{
			calcul_cout_tour(P, S1.itineraire[i]);
		}
		calcul_cout(S1); // Calcul du coût total de la solution S1

		// Génération d'un nombre aléatoire entre 0 et 1 pour choisir une action
		std::uniform_real_distribution<double> distribution(0.0, 1.0);
		proba = distribution(mt_engine);

		// Choix de la fonction a utiliser basée sur le nombre aléatoire généré
		choix = (proba < 0.25) ? 0 : ((proba < 0.5) ? 1 : ((proba < 0.75) ? 2 : 3));


		// Appeler une fonction en fonction du nombre aleatoire genere
		switch (choix) {
		case 0:
			// Selection aleatoire d'une tournee
			tournee_index = (std::rand() % S1.nb_tournee) + 1;
			while (S1.itineraire[tournee_index].nb_client_tour < 2)
				tournee_index = (std::rand() % S1.nb_tournee) + 1;

			// Appliquer la fonction de deplacement sur la tournee selectionnee
			verif_tour(S1.itineraire[tournee_index]);
			deplacement_sommet(P, S1.itineraire[tournee_index]);
			
			break;
		case 1:
			// Selection aleatoire d'une tournee
			tournee_index = (std::rand() % S1.nb_tournee) + 1;
			while (S1.itineraire[tournee_index].nb_client_tour < 3)
				tournee_index = (std::rand() % S1.nb_tournee) + 1;

			// Appliquer la fonction deux_opt_une_tournee sur la tournee selectionnee
			verif_tour(S1.itineraire[tournee_index]);
			deux_opt_une_tournee(P, S1.itineraire[tournee_index]);
			
			break;
		case 2:
			// Selection aleatoire de deux tournees
			tournee_index_1 = (std::rand() % S1.nb_tournee) + 1;
			while (S1.itineraire[tournee_index_1].nb_client_tour < 2)
				tournee_index_1 = (std::rand() % S1.nb_tournee) + 1;
			tournee_index_2 = (std::rand() % S1.nb_tournee) + 1;
			while (tournee_index_2 == tournee_index_1)
				tournee_index_2 = (std::rand() % S1.nb_tournee) + 1;

			// Appliquer la fonction insert_inter_tournee sur les tournees selectionnees
			insert_inter_tournee(P, S1.itineraire[tournee_index_1], S1.itineraire[tournee_index_2]);
			break;
		case 3:
			tournee_index_1 = (std::rand() % S1.nb_tournee) + 1;
			while (S1.itineraire[tournee_index_1].nb_client_tour < 2)
				tournee_index_1 = (std::rand() % S1.nb_tournee) + 1;
			tournee_index_2 = (std::rand() % S1.nb_tournee) + 1;
			while ((S1.itineraire[tournee_index_2].nb_client_tour < 2) || (tournee_index_2 == tournee_index_1))
				tournee_index_2 = (std::rand() % S1.nb_tournee) + 1;

			// Appliquer la fonction deux_opt_inter_tournee sur les tournées sélectionnées
			deux_opt_inter_tournee(P, S1.itineraire[tournee_index_1], S1.itineraire[tournee_index_2]);
			break;
		default:
			break;
		}
		

		calcul_cout(S1); // Recalcul du coût total après application de l'action

		// Si la nouvelle solution S1 est meilleure que l'ancienne solution S, la mettre à jour
		if (S1.total_cout < S.total_cout) {
			tour_to_solution(P, S1); // Convertir les tournées en solution
			S = S1; // Mettre à jour la solution actuelle avec la nouvelle solution S1
			std::cout << "===============================================\n" << std::endl;
			std::cout << "Nouvelle Solution apres optimisation des tours : " << std::endl;
			affiche_vecteur(S, P.nb_de_client); // Afficher la nouvelle solution
			std::cout << "Cout : " << S.total_cout << " km(s)" << endl; // Afficher le coût total
		}

	}
} 



 /**
  * \brief Affiche les détails de la solution du Problème de Routage de Véhicules (VRP).
  * \param P La structure de données t_probleme contenant les détails du problème.
  * \param S La structure de données t_solution contenant la solution à afficher.
  */
void afficher_solution(t_probleme P, t_solution S)
{
	int n; // Variable pour stocker le nombre de clients dans une tournée
	cout << "Nombre de tournees : " << S.nb_tournee << endl; // Affichage du nombre de tournées dans la solution
	int camion = P.nb_vehicule; // Nombre total de camions disponibles dans le problème

	// Affichage des détails pour chaque tournée dans la solution
	for (int i = 1; i <= S.nb_tournee; i++)
	{
		n = S.itineraire[i].nb_client_tour; // Nombre de clients dans la tournée actuelle
		cout << "Clients : ";

		// Affichage des clients dans la tournée actuelle
		for (int j = 1; j <= n; j++)
		{
			cout << S.itineraire[i].clients[j] << " "; // Affichage de chaque client dans la tournée
		}

		cout << "\nCout de la tournee n." << i << " : " << S.itineraire[i].cout << endl; // Affichage du coût de la tournée actuelle
		cout << endl;
		camion--; // Décrémentation du nombre de camions restants à afficher
	}

	cout << "Cout total : " << S.total_cout << endl; // Affichage du coût total de la solution
	cout << endl;
}
