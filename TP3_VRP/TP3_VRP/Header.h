#pragma once
#ifndef HEADER_H
#define HEADER_H

#include <iostream>
#include <string>
#include <fstream>
#include <sstream>
#include <random>
#include <chrono>
#include <cstdlib>
#include <ctime>
#include <vector>   // Pour utiliser std::vector
#include <algorithm> // Pour utiliser std::sort


using namespace std;

// Définition de la taille maximale du nombre de clients et de la valeur "infini"
const int nb_max_client = 200; // Limite maximale du nombre de clients (120 pour pdd)
const int infini = 99999; // Valeur représentant l'infini
const int nb_max = 50; // Nombre maximal de véhicules
const int it_max = 100; // Nombre maximal d'itérations
const int taille_liste = 6; // Taille maximale de la liste

// Structure de données représentant le problème de routage de véhicules
typedef struct t_probleme {
    int nb_de_client; // Nombre total de clients
    int capa_vehicule; // Capacité maximale des véhicules
    int nb_vehicule; // Nombre total de véhicules
    int distance[nb_max_client][nb_max_client]; // Matrice des distances entre les clients
    int quantite[nb_max_client]; // Tableau représentant les quantités associées à chaque client
} t_probleme;

// Structure de données représentant une tournée
typedef struct t_tour {
    int cout; // Coût total de la tournée
    int nb_client_tour = 0; // Nombre de clients dans la tournée
    int clients[nb_max]; // Vecteur de la liste de clients
    int capacite[nb_max]; // Capacité de chaque client
} t_tour;

typedef struct t_label {
    int nb_vehicule; // Nombre de camions restants
    int cout; // Coût du label
    int sommet_pere; // Sommet père du label
    int position_pere; // Position du label père dans la liste de labels du sommet père
} t_label;

typedef struct liste_label {
    int nb_label; // Nombre total de labels dans la liste
    t_label label[nb_max]; // Tableau de labels
} t_list_label;

// Structure de données représentant une solution au problème de routage de véhicules
typedef struct t_solution {
    int total_cout; // Coût total de la solution
    t_tour itineraire[nb_max]; // Itinéraire de chaque tournée
    int nb_tournee; // Nombre total de tournées
    int TSP_solution[nb_max]; // Solution pour le problème du voyageur de commerce
    t_list_label labels[nb_max]; // Liste de labels associée à la solution
} t_solution;

// Structure de données représentant un couple (client, distance)
typedef struct t_couple {
    int client; // Identifiant du client
    int distance; // Distance associée au client
} t_couple;

// Structure de données pour une liste de couples
typedef struct t_liste {
    int n; // Nombre d'éléments dans la liste
    t_couple T[taille_liste + 2]; // Tableau de couples
} t_liste;




// Declarations des fonctions et procedures

void lire_fichier_instance(t_probleme& P, string nom);

int verif_tab_client(int tab_client[], int client, int taille);

void plus_proche_voisin(t_probleme& P, t_solution& S);

void affiche_vecteur(t_solution& S, int taille);
void affiche_tournee(t_tour& T);

void poser(t_liste& liste, t_couple element);

int cherche_idx(int tab_client[], int taille, int client);

void plus_proche_voisin_randomise(t_probleme& P, t_solution& S);

void clarke_wright(t_probleme& P, t_solution& S);

int calcul_cout_TSP(t_probleme& P, t_solution& S);
void calcul_cout_tour(t_probleme& P, t_tour& T);
void calcul_cout(t_solution& S);

void deux_opt_une_tournee(t_probleme& P, t_tour& T);
void deux_opt_inter_tournee(t_probleme& P, t_tour& T1, t_tour& T2);

void deplacement_sommet(t_probleme& P, t_tour& T);
void insert_inter_tournee(t_probleme& P, t_tour& T1, t_tour& T2);

void verif_tour(t_tour& T);
void tour_to_solution(t_probleme& P, t_solution& S);

int domine(t_label P, t_label L);
int ajoute_label(t_list_label liste, t_label l);

void split(t_probleme& P, t_solution& S);
t_solution split_inverse(t_probleme P, t_solution& S);
void Split_Vehicule_Fini(t_probleme& T, t_solution& S);

void afficher_solution(t_probleme P, t_solution S);

void Recherche_TSP(t_probleme P, t_solution& S, int nb_iter);
void Recherche_VRP(t_probleme P, t_solution& S, int nb_iter);

#endif
