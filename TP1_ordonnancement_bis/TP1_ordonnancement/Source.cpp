#include"Header.h"
#include <iostream>
#include <string>
#include <fstream>

void lire_fichier_excel(t_graphe& g, std::string nom_fichier) {
	
	std::ifstream fichier_ouvert(nom_fichier.c_str());

	if (fichier_ouvert.is_open())
	{
		fichier_ouvert >> g.nb_sommet;

		int nb_ligne = 0;
		int k;
		// initialiser nb_successeur[]

		// faire *100 pour passer float en int
		
		fichier_ouvert >> nb_ligne;

		for (int i = 1; i <= nb_ligne; i++)
		{
			int sommet_origine = 0;
			int sommet_final = 0;
			int duree = 0;
			int valeur_non_utilisee = 0;
			fichier_ouvert >> sommet_origine;
			fichier_ouvert >> sommet_final;
			fichier_ouvert >> duree;
			fichier_ouvert >> valeur_non_utilisee;


			g.nb_successeur[sommet_origine]++;
			k = g.nb_successeur[sommet_origine];
			g.successeur[sommet_origine][k] = sommet_final;
			g.longueur_arc[sommet_origine][k] = duree;
			
		}
		fichier_ouvert.close();
	}
	else
	{
		std::cout << "Ouverture ratee" << std::endl;
	}
}

void lire_fichier(t_graphe& g, std::string nom_fichier)   //type standard std::string
{
	int sommet = 0;
	int valeur = 0;
	std::ifstream fichier_ouvert(nom_fichier.c_str());

	if (fichier_ouvert.is_open())
	{
		fichier_ouvert >> g.nb_sommet;

		for (int i = 1; i <= g.nb_sommet; i++)
		{
			fichier_ouvert >> g.nb_successeur[i];
			for (int j = 1; j <= g.nb_successeur[i]; j++)
			{
				fichier_ouvert >> sommet;
				fichier_ouvert >> valeur;
				g.successeur[i][j] = sommet;
				g.longueur_arc[i][j] = valeur;
			}
		}
		fichier_ouvert.close();
	}
	else
	{
		std::cout << "Ouverture ratee" << std::endl;
	}
}



void calculer_plus_court_chemin(t_graphe& g, int sommet_d�part, int sommet_final)
{
	int traite[nb_max_sommet];  // Liste des sommets trait�s

	// Etape 1 : Initatilisation 
	for (int i = 1; i <= g.nb_sommet; i++)
	{
		g.marque_sommet[i] = c_infini;
		traite[i] = 0;
	}

	g.marque_sommet[sommet_d�part] = 0; // On cherche le plus court chemin � partir d'un sommet de d�part

	int nb = g.nb_sommet;
	int valeur_min = c_infini;
	int pos_min = -1;
	int nbsucc = 0;
	int valeur_arc = 0;
	int sommet = 0;

	for (int i = 1; i <= nb; i++)
	{
		// Recherche du sommet ayant la marque la plus petite
		valeur_min = c_infini;
		pos_min = -1;

		for (int j = 1; j <= nb; j++)
		{
			if (traite[j] == 0)
			{
				if (g.marque_sommet[j] < valeur_min)
				{
					valeur_min = g.marque_sommet[j];
					pos_min = j;
				}
			}
		}

		// Calcul des marques des successeurs
		nbsucc = g.nb_successeur[pos_min];

		for (int k = 1; k <= nbsucc; k++)
		{
			valeur_arc = g.longueur_arc[pos_min][k];
			sommet = g.successeur[pos_min][k];
			if (valeur_min + valeur_arc < g.marque_sommet[sommet])
			{
				g.marque_sommet[sommet] = valeur_min + valeur_arc;
				g.pere[sommet] = pos_min;
			}
		}
		// On arque le sommet comme traite
		traite[pos_min] = 1;
	}
}

void calculer_plus_court_chemin_bellman(t_graphe& g, int sommet_depart, int sommet_fin)
{
	// �tape 1
	for (int i = 1; i <= g.nb_sommet; i++)
	{
		g.marque_sommet[i] = c_infini;
	}

	g.marque_sommet[sommet_depart] = 0;

	int stop = 0;
	int compteur = 0;
	while (stop==0) 
	{
		stop = 1;
		compteur++;



		int nb = g.nb_sommet;
		for (int i = 1; i <= nb; i++)
		{
			// prochain sommet � traiter
			int pos_min = g.ordre[i];
			int valeur_min = g.marque_sommet[pos_min];

			// marquer tous les successeurs
			int nbsucc = g.nb_successeur[pos_min];

			for (int k = 1; k <= nbsucc; k++)
			{
				int valeur_arc = g.longueur_arc[pos_min][k];
				int sommet = g.successeur[pos_min][k];
				if (valeur_min + valeur_arc < g.marque_sommet[sommet])
				{
					g.marque_sommet[sommet] = valeur_min + valeur_arc;
					g.pere[sommet] = pos_min;
					stop = 0;
				}
			}

		}
	}
	std::cout << compteur << std::endl;
}