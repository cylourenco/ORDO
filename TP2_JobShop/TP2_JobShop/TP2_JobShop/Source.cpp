#include "Header.h"
#include <random>

void lire_fichier_instance(t_probleme& P, string nom)
{
	int machine = 0;
	int duree = 0;
	std::ifstream fichier_ouvert(nom.c_str());

	if (fichier_ouvert.is_open())
	{
		fichier_ouvert >> P.nb_jobs;
		fichier_ouvert >> P.nb_machines;

		for (int i = 1; i < P.nb_jobs+1; i++)
		{
			for (int j = 1; j < P.nb_machines+1; j++)
			{
				fichier_ouvert >> machine;
				fichier_ouvert >> duree;
				P.machines[i][j] = machine;
				P.duree_operations[i][j] = duree;
			}
		}
		fichier_ouvert.close();
	}
	else
	{
		std::cout << "Ouverture ratee" << std::endl;
	}
}

void lire_fichier_opt(int opt[nb_max_job], string nom)
{
	std::ifstream fichier_ouvert(nom.c_str());
	int nb;
	int tmp;

	if (fichier_ouvert.is_open())
	{
		fichier_ouvert >> nb;

		for (int i = 1; i < nb + 1; i++)
		{
			fichier_ouvert >> tmp;
			opt[i] = tmp;
		}
		fichier_ouvert.close();
	}
	else
	{
		std::cout << "Ouverture ratee" << std::endl;
	}
}


void generer_vecteur(t_probleme P, t_solution& S)
{
	// 10 fois 5 machines
	// tirer al�atoirement

	// tableau taille 5 : 10 dans chaque case : d�s que c'est pioch� al�atoirement (entre 0 et 4)
	// on d�cr�mente la case

	int tab1[nb_max_job];
	int tab2[nb_max_job];
	int alea;
	int taille_tab = P.nb_jobs;
	int max_rand = P.nb_jobs;


	// initialisation tableau
	for (int i = 1; i < P.nb_jobs+1; i++) {
		tab1[i] = i;
		tab2[i] = P.nb_machines;
	}

	// al�atoire
	std::srand(std::time(nullptr));
	for (int i = 1; i < P.nb_machines * P.nb_jobs + 1; i++)
	{
		do {
			alea = (rand() % max_rand) + 1;
		} while (alea == 0);
		
		tab2[alea]--;

		S.vecteur[i] = tab1[alea];

		if (tab2[alea] <= 0)
		{
			tab1[alea] = tab1[taille_tab];
			tab2[alea] = tab2[taille_tab];
			taille_tab--;
			max_rand--;
		}

	}

}

int tester_vecteur(t_probleme P, t_solution& S)
{
	int compteur[nb_max_job];
	int retour = 1;

	// initialisation
	for (int i = 1; i < P.nb_jobs+1; i++)
	{
		compteur[i] = P.nb_machines;
	}

	// parcours du vecteur
	for (int i = 1; i < P.nb_jobs * P.nb_machines + 1; i++)
	{
		compteur[S.vecteur[i]]--;
	}

	// test vecteur
	for (int i = 1; i < P.nb_jobs + 1; i++)
	{
		if (compteur[i] != 0)
			retour = 0;
	}

	return retour;
}

void afficher_machine(t_probleme P)
{
	for (int i = 1; i < P.nb_jobs + 1; i++)
	{
		for (int k = 1; k < P.nb_machines + 1; k++)
		{
			cout << P.machines[i][k] << " ";
			cout << P.duree_operations[i][k] << " ";
		}
		cout << endl;
	}
}

// 3 3 1 2 2 3 2 1 1 -> arcs conjonctifs de m3 à m2 car sur la même pièce
//m3 2 1 2 1 1 3 2 3 -> arcs dijonctifs entre les machines de même numéro

void evaluer_solution(t_probleme P, t_solution& S)
{
	int nJob[nb_max_job] = { 0 };
	int date, duree, new_cout;
	int nl, nc;
	int machine = 0;

	t_couple nMachine[nb_max_machine];

	int j;

	for (int i = 1; i < P.nb_jobs+1; i++)
	{
		for (int k = 1; k < P.nb_machines+1; k++)
		{
			S.date[i][k] = 0;
		}
	}

	for (int i = 1; i < P.nb_machines+1; i++)
	{
		nMachine[i].numero_piece = 0;
		nMachine[i].rang_gamme = 0;
	}

	for (int i = 1; i < P.nb_jobs * P.nb_machines + 1; i++)
	{
		j = S.vecteur[i];
		(nJob[j])++;

		// Arc Conjonctifs
		// on ne veut pas que les arcs de 0 aux premières machines soient traitées
		if (nJob[j] > 1)
		{
			date = S.date[j][nJob[j] - 1];
			duree = P.duree_operations[j][nJob[j] - 1];
			if (date + duree > S.date[j][nJob[j]])
			{
				S.date[j][nJob[j]] = date + duree;
				S.pere[j][nJob[j]].numero_piece = j;
				S.pere[j][nJob[j]].rang_gamme = nJob[j] - 1;
			}

		}
		else
		{
			// on met le pere du rang 1 à 0
			S.pere[j][nJob[j]].rang_gamme = 0;
		}

		// Arc Dijonctifs

		machine = P.machines[j][nJob[j]];

			
		if (nMachine[machine+1].rang_gamme != 0) // si le rang vaut 0 -> le tt vaut 0
		{
			nl = nMachine[machine+1].numero_piece;
			nc = nMachine[machine+1].rang_gamme;

			if ((S.date[nl][nc] + P.duree_operations[nl][nc]) > S.date[j][nJob[j]])
			{
				S.date[j][nJob[j]] = (S.date[nl][nc] + P.duree_operations[nl][nc]);

				S.pere[j][nJob[j]].numero_piece = nl;
				S.pere[j][nJob[j]].rang_gamme = nc;

			}
		}
		nMachine[machine+1].numero_piece = j;
		nMachine[machine+1].rang_gamme = nJob[j];

	
	}
	// cout est egal au dernier temps de la piece au plus tot plus le temps que la machine met pour aller de cette piece à *
	// on se trouve sur la dernière machine de la pièce donc -> nb_max_machines

	S.pere[P.nb_jobs+1][P.nb_machines+1].numero_piece = 0;
	S.pere[P.nb_jobs+1][P.nb_machines + 1].rang_gamme = P.nb_machines;
	S.cout = 0;
	// on compare toutes les pieces entre elles pour voir quel chemin prendre
	for (int i = 1; i < P.nb_jobs + 1; i++)
	{
		new_cout = S.date[i][P.nb_machines] + P.duree_operations[i][P.nb_machines];
		if (S.cout < new_cout)
		{
			S.cout = new_cout;
			S.pere[P.nb_jobs + 1][P.nb_machines + 1].numero_piece = i;

		}
	}

}

void affiche_pere(t_probleme P, t_solution& S) {
	for (int i = 1; i < P.nb_jobs+1; i++)
	{
		for (int k = 1; k < P.nb_machines+1; k++)
		{
			cout << i << " " << k << " " << S.pere[i][k].numero_piece << "," << S.pere[i][k].rang_gamme << " " << endl;
		}
		cout << "\n" << endl;
	}
}

// recherche du chemin +long ayant le cout le + bas
void recherche_locale(t_probleme P, t_solution& S, int nb)
{

	// on va remonter le scénario (on part de *)
	// on compare les solutions et on repart à *
	evaluer_solution(P, S);

	t_solution S2;


	int iter = 0;
	// l'arc allant jusqu'à * ne changera jms donc on part de son pere
	t_couple i = S.pere[P.nb_jobs + 1][P.nb_machines + 1];
	// pere de i
	t_couple j = S.pere[i.numero_piece][i.rang_gamme];

	while ((iter <= nb) && (j.rang_gamme != 0))
	{
		S2 = S;

		// on regarde si i et j ont un arc conjonctif == viennent de la même pièce
		if (j.numero_piece == i.numero_piece)
		{
			i = j;
			j = S.pere[j.numero_piece][j.rang_gamme];
		}
		else {
			// boucle pour chercher lesquelles permuter dans le vecteur
			int indice = 1;
			int compteur1 = 0;
			int compteur2 = 0;
			int ind1 = 0;
			int ind2 = 0;
			int temp = 0; // indices du vecteur pour permuter 
			while (ind1 == 0)
			{
				if (S.vecteur[indice] == i.numero_piece)
					compteur1++;
				if (compteur1 == i.rang_gamme)
					ind1 = indice;
				indice++;
			}
			indice = 1;
			while (ind2 == 0)
			{
				if (S.vecteur[indice] == j.numero_piece)
					compteur2++;
				if (compteur2 == j.rang_gamme)
					ind2 = indice;
				indice++;
			}
			// permutation
			temp = S2.vecteur[ind1];
			S2.vecteur[ind1] = S2.vecteur[ind2];
			S2.vecteur[ind2] = temp;

			evaluer_solution(P, S2);

			if (S2.cout < S.cout)
			{
				// on a trouvé un graphe de cout inferieur -> le chemin critique a changé
				S = S2;
				// on repart alors tout à droite
				i = S.pere[P.nb_jobs + 1][P.nb_machines + 1];
				j = S.pere[i.numero_piece][i.rang_gamme];
			}
			else // permutation perdante
			{
				// on continue de remonter le chemin
				i = j;
				j = S.pere[j.numero_piece][j.rang_gamme];
			}

		}
		iter++;
	} // on arrête - on a un graphe optimal
	//cout << "Le cout du plus long chemin optimal avec recherche_local est : " << S.cout << endl;
}


// c'est juste un test pour trouver le meilleur vecteur -> pas sûr que ça fonctionne
void essai_vecteur(t_probleme P, t_solution &S)
{
	generer_vecteur(P, S);
	recherche_locale(P, S, 100);

	t_solution S_opt = S;
	for (int i = 0; i < 100; i++)
	{
		generer_vecteur(P, S);
		recherche_locale(P, S, 100);
		// on garde le cout optimal 
		if (S.cout < S_opt.cout)
			S_opt = S;
	}	

	cout << "Le cout du plus long chemin optimal est : " << S_opt.cout << endl;
}


t_solution genere_voisin(t_solution S, t_probleme P)
{
	t_solution S2 = S;

	// on inverse aléatoirement
	int index1, index2;

    index1 = 1 + rand() % (P.nb_jobs*P.nb_machines);
    index2 = 1 + rand() % (P.nb_jobs*P.nb_machines);

    // Assurez-vous que les indices sont différents
    while (index1 == index2) {
        index2 = 1 + rand() % 5;
    }

    // Inversez les deux chiffres
    int temp = S.vecteur[index1];
    S2.vecteur[index1] = S2.vecteur[index2];
    S2.vecteur[index2] = temp;

	return S2;
}

int f_hash(t_probleme P, t_solution &S)
{
	int somme = 0;
	for (int i = 1; i <=P.nb_jobs; i++)
		for (int j = 1; j <=P.nb_machines; j++)
			somme += S.date[i][j]*S.date[i][j];

	return somme/taille_tab;
}

void GRASP(t_probleme P, t_solution &S, int nb_rech, int nb_cascade, int nb_voisins)
{
	int T[taille_tab] = { 0 };
	
	t_solution S2 = S; // pour ne pas modifier S

	recherche_locale(P, S2, nb_rech);
	t_solution S_opt_voisin = S2;
	T[f_hash(P, S2)] = 1;

	// on a au départ le vecteur : S.vecteur
	// on génère un voisin aléatoirement avec une fonction

	int iter_cascade = 0;
	int iter_voisin = 0;

	while (iter_cascade < nb_cascade)
	{
		// boucle sur les voisins 
		while(iter_voisin < nb_voisins)
		{
			t_solution voisin = genere_voisin(S2, P);

			// on cherche un voisin qui n'a pas été utilisé auparavant
			if (T[f_hash(P, S2)] == 0)
				voisin = genere_voisin(S2, P);
				recherche_locale(P, voisin, nb_rech);
			
				// mise à jour du tableau
				T[f_hash(P, voisin)] = 1;

				// on compare
				if (voisin.cout < S_opt_voisin.cout)
					S_opt_voisin = voisin;
			
				iter_voisin++;
		}

		S2 = S_opt_voisin;

		iter_cascade++;
	}
	cout << "Le cout du plus long chemin optimal avec GRASP est : " << S_opt_voisin.cout << endl;
}
