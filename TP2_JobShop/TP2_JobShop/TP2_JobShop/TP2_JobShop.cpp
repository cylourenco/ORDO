// TP2_JobShop.cpp : Ce fichier contient la fonction 'main'. L'exécution du programme commence et se termine à cet endroit.
//

#include <iostream>
#include "Header.h"

int main()
{
    t_probleme P;
    t_solution S;
    int opt[nb_max_job];
    lire_fichier_opt(opt, "opt.txt");

    for (int i = 1; i < 22; i++)
    {
        string s = (i < 10 ? "0" : "") + to_string(i);
        string filename = "./laxxx/la" + s + ".txt";
        lire_fichier_instance(P, filename);

        cout << "lax :  " << s << endl;
        cout << "nb de job :  " << P.nb_jobs << endl;
        cout << "nb de machine :  " << P.nb_machines << endl;

        generer_vecteur(P, S);

        if (tester_vecteur(P, S) == 1)
        {
            cout << "Le vecteur est correct" << endl;

            GRASP(P, S, 100, 100, 100);

            cout << "Optimale :  " << opt[i] << endl;
            cout << "" << endl;
        }
        else
        {
            cout << "Le vecteur est incorrect" << endl;
        }


    }

    /*version 1*/
    lire_fichier_instance(P, "./laxxx/la01.txt");
    cout << "nb de job :  " << P.nb_jobs  << endl;
    cout << "nb de machine :  " << P.nb_machines << endl;

    
    generer_vecteur(P, S);

    // vecteur de thomas pour tester si on a pareil que lui 
    int idx = 1; S.vecteur[idx++] = 4; S.vecteur[idx++] = 0; S.vecteur[idx++] = 7; S.vecteur[idx++] = 6; S.vecteur[idx++] = 3; S.vecteur[idx++] = 3; S.vecteur[idx++] = 8; S.vecteur[idx++] = 5; S.vecteur[idx++] = 4; S.vecteur[idx++] = 9; S.vecteur[idx++] = 6; S.vecteur[idx++] = 1; S.vecteur[idx++] = 4; S.vecteur[idx++] = 8; S.vecteur[idx++] = 0; S.vecteur[idx++] = 9; S.vecteur[idx++] = 2; S.vecteur[idx++] = 6; S.vecteur[idx++] = 3; S.vecteur[idx++] = 7; S.vecteur[idx++] = 7; S.vecteur[idx++] = 5; S.vecteur[idx++] = 1; S.vecteur[idx++] = 5; S.vecteur[idx++] = 3; S.vecteur[idx++] = 1; S.vecteur[idx++] = 9; S.vecteur[idx++] = 5; S.vecteur[idx++] = 8; S.vecteur[idx++] = 2; S.vecteur[idx++] = 0; S.vecteur[idx++] = 7; S.vecteur[idx++] = 6; S.vecteur[idx++] = 8; S.vecteur[idx++] = 9; S.vecteur[idx++] = 5; S.vecteur[idx++] = 1; S.vecteur[idx++] = 6; S.vecteur[idx++] = 7; S.vecteur[idx++] = 8; S.vecteur[idx++] = 3; S.vecteur[idx++] = 2; S.vecteur[idx++] = 0; S.vecteur[idx++] = 1; S.vecteur[idx++] = 4; S.vecteur[idx++] = 2; S.vecteur[idx++] = 9; S.vecteur[idx++] = 0; S.vecteur[idx++] = 2; S.vecteur[idx++] = 4;
    for (int i = 1; i < 51; i++)
        S.vecteur[i] ++;
    cout << "Le vecteur est correct : " << tester_vecteur(P, S) << endl;

    //afficher_machine(P);
    /*
    S.vecteur[1] = 2;
    S.vecteur[2] = 1;
    S.vecteur[3] = 3;

    S.vecteur[4] = 2;
    S.vecteur[5] = 1;
    S.vecteur[6] = 3;

    S.vecteur[7] = 2;
    S.vecteur[8] = 1;
    S.vecteur[9] = 3;
    */

    //recherche_locale(P, S, 700);
    GRASP(P, S, 100, 100, 100);


}

// Exécuter le programme : Ctrl+F5 ou menu Déboguer > Exécuter sans débogage
// Déboguer le programme : F5 ou menu Déboguer > Démarrer le débogage

// Astuces pour bien démarrer : 
//   1. Utilisez la fenêtre Explorateur de solutions pour ajouter des fichiers et les gérer.
//   2. Utilisez la fenêtre Team Explorer pour vous connecter au contrôle de code source.
//   3. Utilisez la fenêtre Sortie pour voir la sortie de la génération et d'autres messages.
//   4. Utilisez la fenêtre Liste d'erreurs pour voir les erreurs.
//   5. Accédez à Projet > Ajouter un nouvel élément pour créer des fichiers de code, ou à Projet > Ajouter un élément existant pour ajouter des fichiers de code existants au projet.
//   6. Pour rouvrir ce projet plus tard, accédez à Fichier > Ouvrir > Projet et sélectionnez le fichier .sln.
