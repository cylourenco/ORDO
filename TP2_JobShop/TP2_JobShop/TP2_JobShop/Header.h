#pragma once
#ifndef HEADER_H
#define HEADER_H

#include <iostream>
#include <string>
#include <fstream>
#include <sstream>
using namespace std;

const int nb_max_job = 30;
const int nb_max_machine = 30;
const int taille_tab = 1000;


typedef struct t_probleme {
	int nb_jobs;
	int nb_machines;
	int duree_operations[nb_max_job][nb_max_machine];
	int machines[nb_max_job][nb_max_machine];
}t_probleme;

typedef struct t_couple {
	int numero_piece;
	int rang_gamme;
}t_couple;

typedef struct t_solution {
	int cout;
	int vecteur[nb_max_job * nb_max_machine];
	t_couple pere[nb_max_job][nb_max_machine];
	int date[nb_max_job][nb_max_machine]; // date de d�but au plus t�t des op�rations = St
}t_solution;


void lire_fichier_instance(t_probleme& P, string nom);

void lire_fichier_opt(int opt[nb_max_job], string nom);

void generer_vecteur(t_probleme P, t_solution& S);

int tester_vecteur(t_probleme P, t_solution& S);

void evaluer_solution(t_probleme P, t_solution& S);

void recherche_locale(t_probleme P, t_solution& S, int nb);

void essai_vecteur(t_probleme P, t_solution& S);

void afficher_machine(t_probleme P);

void affiche_pere(t_probleme P, t_solution& S);

int genere_voisin(int vecteur[]);

int f_hash(t_probleme P, t_solution &S);

void GRASP(t_probleme P, t_solution &S, int nb_rech, int nb_cascade, int nb_voisins);


#endif
