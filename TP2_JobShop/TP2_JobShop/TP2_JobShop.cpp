// TP2_JobShop.cpp : Ce fichier contient la fonction 'main'. L'exécution du programme commence et se termine à cet endroit.
//


#include "Header.h"

int main()
{
t_probleme P;
t_solution S;

int opt[nb_max_job],     // Tableau pour stocker les valeurs optimales
    grasp,               // Coût de la solution GRASP
    nb_lax = 20,         // Nombre d'instances du problème à résoudre
    precision = 1000;    // Précision de la recherche GRASP

double  avg_diff = 0,    // Variable pour stocker la moyenne des différences par rapport à l'optimal
        avg_time = 0,    // Variable pour stocker la moyenne du temps d'exécution
        diff;            // Différence par rapport à l'optimal

lire_fichier_opt(opt, "opt.txt");  // Lecture des valeurs optimales à partir d'un fichier

for (int i = 1; i < nb_lax + 1; i++)
{
    string s = (i < 10 ? "0" : "") + to_string(i);
    string filename = "./laxxx/la" + s + ".txt";
    lire_fichier_instance(P, filename);  // Lecture d'une instance du problème

    cout << "lax :  " << s << endl;
    cout << "nb de job :  " << P.nb_jobs << endl;
    cout << "nb de machine :  " << P.nb_machines << endl;

    generer_vecteur(P, S);  // Génération d'une solution initiale

    if (tester_vecteur(P, S) == 1)  // Vérification de la validité de la solution initiale
    {
        auto start_time = chrono::high_resolution_clock::now();
  
        cout << "Le vecteur est correct" << endl;

        grasp = GRASP(P, S, precision, precision, precision);  // Application de GRASP pour résoudre le problème

        cout << "Le cout du plus long chemin optimal avec GRASP est : " << grasp << endl;

        cout << "Optimale :  " << opt[i] << endl;
        diff = (grasp - opt[i]) / (double)opt[i];
        cout << "Ecart relatif entre GRASP et l'optimale : " << diff << endl;

        auto end_time = chrono::high_resolution_clock::now();
        chrono::duration<double> elapsed_time = end_time - start_time;

        cout << "Temps écoulé : " << elapsed_time.count() << " secondes" << endl;

        avg_diff += diff;
        avg_time += elapsed_time.count();

        cout << "" << endl;
    }
    else
    {
        cout << "Le vecteur est incorrect" << endl;
        exit(-1);  // Sortir avec un code d'erreur en cas de vecteur incorrect
    }
}

cout << "Différence moyenne entre GRASP et l'optimale : " << avg_diff / nb_lax << endl;
cout << "Temps moyen : " << avg_time / nb_lax << endl;


}

// Exécuter le programme : Ctrl+F5 ou menu Déboguer > Exécuter sans débogage
// Déboguer le programme : F5 ou menu Déboguer > Démarrer le débogage

// Astuces pour bien démarrer : 
//   1. Utilisez la fenêtre Explorateur de solutions pour ajouter des fichiers et les gérer.
//   2. Utilisez la fenêtre Team Explorer pour vous connecter au contrôle de code source.
//   3. Utilisez la fenêtre Sortie pour voir la sortie de la génération et d'autres messages.
//   4. Utilisez la fenêtre Liste d'erreurs pour voir les erreurs.
//   5. Accédez à Projet > Ajouter un nouvel élément pour créer des fichiers de code, ou à Projet > Ajouter un élément existant pour ajouter des fichiers de code existants au projet.
//   6. Pour rouvrir ce projet plus tard, accédez à Fichier > Ouvrir > Projet et sélectionnez le fichier .sln.
