#pragma once
#ifndef HEADER_H
#define HEADER_H

#include <iostream>
#include <string>
#include <fstream>
#include <sstream>
#include <random>
#include <chrono>

using namespace std;

/**
 * @file Header.h
 * @brief Ce fichier contient les déclarations des structures de données et des fonctions pour résoudre un problème d'ordonnancement.
 */

/**
 * @brief Constante pour le nombre maximal de jobs.
 */
const int nb_max_job = 30;

/**
 * @brief Constante pour le nombre maximal de machines.
 */
const int nb_max_machine = 30;

/**
 * @brief Constante pour la taille maximale du tableau.
 */
const int taille_tab = 100000;

/**
 * @brief Structure de données pour le problème d'ordonnancement.
 */
typedef struct t_probleme {
    int nb_jobs;                          ///< Nombre de jobs dans le problème.
    int nb_machines;                     ///< Nombre de machines dans le problème.
    int duree_operations[nb_max_job][nb_max_machine]; ///< Tableau pour stocker la durée des opérations.
    int machines[nb_max_job][nb_max_machine];        ///< Tableau pour stocker les machines.
} t_probleme;

/**
 * @brief Structure de données pour un couple.
 */
typedef struct t_couple {
    int numero_piece; ///< Numéro de la pièce.
    int rang_gamme;  ///< Rang de gamme de la pièce.
} t_couple;

/**
 * @brief Structure de données pour une solution du problème d'ordonnancement.
 */
typedef struct t_solution {
    int cout;                           ///< Coût de la solution.
    int vecteur[nb_max_job * nb_max_machine]; ///< Vecteur de jobs affectés aux machines.
    t_couple pere[nb_max_job][nb_max_machine]; ///< Tableau de couples père.
    int date[nb_max_job][nb_max_machine]; ///< Tableau de dates de début au plus tôt des opérations (St).
} t_solution;

/**
 * @brief Lit les données d'une instance depuis un fichier.
 *
 * @param P Structure de données du problème.
 * @param nom Nom du fichier d'instance.
 */
void lire_fichier_instance(t_probleme& P, string nom);

/**
 * @brief Lit les données de la solution optimale depuis un fichier.
 *
 * @param opt Tableau de la solution optimale.
 * @param nom Nom du fichier de la solution optimale.
 */
void lire_fichier_opt(int opt[nb_max_job], string nom);

/**
 * @brief Génère un vecteur de jobs aléatoire pour une solution.
 *
 * @param P Structure de données du problème.
 * @param S Structure de données de la solution.
 */
void generer_vecteur(t_probleme P, t_solution& S);

/**
 * @brief Teste si un vecteur de jobs est valide pour le problème donné.
 *
 * @param P Structure de données du problème.
 * @param S Structure de données de la solution.
 * @return 1 si le vecteur est valide, 0 sinon.
 */
int tester_vecteur(t_probleme P, t_solution& S);

/**
 * @brief Évalue une solution en calculant le coût et les dates de début au plus tôt des opérations.
 *
 * @param P Structure de données du problème.
 * @param S Structure de données de la solution à évaluer.
 */
void evaluer_solution(t_probleme P, t_solution& S);

/**
 * @brief Effectue une recherche locale pour améliorer une solution.
 *
 * @param P Structure de données du problème.
 * @param S Structure de données de la solution à améliorer (passée par référence).
 * @param nb Nombre d'itérations de recherche locale.
 */
void recherche_locale(t_probleme P, t_solution& S, int nb);

/**
 * @brief Effectue une recherche du vecteur optimal.
 *
 * @param P Structure de données du problème.
 * @param S Structure de données de la solution.
 */
void essai_vecteur(t_probleme P, t_solution& S);

/**
 * @brief Affiche les données des machines du problème.
 *
 * @param P Structure de données du problème.
 */
void afficher_machine(t_probleme P);

/**
 * @brief Affiche les données du père des opérations dans la solution.
 *
 * @param P Structure de données du problème.
 * @param S Structure de données de la solution.
 */
void affiche_pere(t_probleme P, t_solution& S);

/**
 * @brief Génère un voisin aléatoire en inversant deux éléments du vecteur de jobs d'une solution.
 *
 * @param S Structure de données de la solution initiale.
 * @param P Structure de données du problème associé.
 * @return Une nouvelle solution voisine.
 */
t_solution genere_voisin(t_solution S, t_probleme P);

/**
 * @brief Calcule la valeur de hachage d'une solution.
 *
 * @param P Structure de données du problème associé.
 * @param S Structure de données de la solution dont on veut calculer la valeur de hachage.
 * @return La valeur de hachage de la solution.
 */
int f_hash(t_probleme P, t_solution& S);

/**
 * @brief Implémente l'algorithme GRASP pour résoudre le problème d'ordonnancement.
 *
 * @param P Structure de données du problème à résoudre.
 * @param S Structure de données de la solution initiale (passée par référence).
 * @param nb_rech Le nombre d'itérations de recherche locale.
 * @param nb_cascade Le nombre d'itérations de cascade.
 * @param nb_voisins Le nombre de voisins générés à chaque itération.
 * @return Le coût de la meilleure solution trouvée.
 */
int GRASP(t_probleme P, t_solution& S, int nb_rech, int nb_cascade, int nb_voisins);


#endif
