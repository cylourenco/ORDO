#include "Header.h"

/**
 * @file Source.c
 * @brief Ce fichier contient les fonctions implémentées pour résoudre un problème d'ordonnancement.
 */


/**
 * @brief Lit les données d'une instance depuis un fichier.
 *
 * @param P Structure de données du problème.
 * @param nom Nom du fichier d'instance.
 */
void lire_fichier_instance(t_probleme& P, string nom)
{
	int machine = 0,
		duree = 0;
	std::ifstream fichier_ouvert(nom.c_str());

	if (fichier_ouvert.is_open())
	{
		fichier_ouvert >> P.nb_jobs;
		fichier_ouvert >> P.nb_machines;

		for (int i = 1; i < P.nb_jobs+1; i++)
		{
			for (int j = 1; j < P.nb_machines+1; j++)
			{
				fichier_ouvert >> machine;
				fichier_ouvert >> duree;
				P.machines[i][j] = machine;
				P.duree_operations[i][j] = duree;
			}
		}
		fichier_ouvert.close();
	}
	else
	{
		std::cout << "Ouverture ratee" << std::endl;
	}
}

/**
 * @brief Lit les données de la solution optimale depuis un fichier.
 *
 * @param opt Tableau de la solution optimale.
 * @param nom Nom du fichier de la solution optimale.
 */
void lire_fichier_opt(int opt[nb_max_job], string nom)
{
	std::ifstream fichier_ouvert(nom.c_str());
	int nb,
		tmp;

	if (fichier_ouvert.is_open())
	{
		fichier_ouvert >> nb;

		for (int i = 1; i < nb + 1; i++)
		{
			fichier_ouvert >> tmp;
			opt[i] = tmp;
		}
		fichier_ouvert.close();
	}
	else
	{
		std::cout << "Ouverture ratee" << std::endl;
	}
}

/**
 * @brief Génère un vecteur de jobs aléatoire pour une solution.
 *
 * @param P Structure de données du problème.
 * @param S Structure de données de la solution.
 */
void generer_vecteur(t_probleme P, t_solution& S)
{

	int tab1[nb_max_job],
		tab2[nb_max_job],
		alea,
		taille_tab = P.nb_jobs,
		max_rand = P.nb_jobs,
		i;

	// initialisation tableau
	for (i = 1; i < P.nb_jobs+1; i++) {
		tab1[i] = i;
		tab2[i] = P.nb_machines;
	}

	// al�atoire
	std::srand(std::time(nullptr));
	for (i = 1; i < P.nb_machines * P.nb_jobs + 1; i++)
	{
		do {
			alea = (rand() % max_rand) + 1;
		} while (alea == 0);
		
		tab2[alea]--;

		S.vecteur[i] = tab1[alea];

		if (tab2[alea] <= 0)
		{
			tab1[alea] = tab1[taille_tab];
			tab2[alea] = tab2[taille_tab];
			taille_tab--;
			max_rand--;
		}

	}

}

/**
 * @brief Teste si un vecteur de jobs est valide pour le problème donné.
 *
 * @param P Structure de données du problème.
 * @param S Structure de données de la solution.
 * @return 1 si le vecteur est valide, 0 sinon.
 */
int tester_vecteur(t_probleme P, t_solution& S)
{
	int compteur[nb_max_job],
		retour = 1,
		i;

	// initialisation
	for (i = 1; i < P.nb_jobs+1; i++)
	{
		compteur[i] = P.nb_machines;
	}

	// parcours du vecteur
	for (i = 1; i < P.nb_jobs * P.nb_machines + 1; i++)
	{
		compteur[S.vecteur[i]]--;
	}

	// test vecteur
	for (i = 1; i < P.nb_jobs + 1; i++)
	{
		if (compteur[i] != 0)
			retour = 0;
	}

	return retour;
}

void afficher_machine(t_probleme P)
{
	int i, k;

	for (i = 1; i < P.nb_jobs + 1; i++)
	{
		for (k = 1; k < P.nb_machines + 1; k++)
		{
			cout << P.machines[i][k] << " ";
			cout << P.duree_operations[i][k] << " ";
		}
		cout << endl;
	}
}


/**
 * @brief Évalue une solution en calculant le coût et les dates de début au plus tôt des opérations.
 *
 * @param P Structure de données du problème.
 * @param S Structure de données de la solution à évaluer.
 */
void evaluer_solution(t_probleme P, t_solution& S)
{
	int nJob[nb_max_job] = { 0 },
		date, duree, new_cout,
		nl, nc,
		machine = 0;

	t_couple nMachine[nb_max_machine];

	int i, j, k;

	for (i = 1; i < P.nb_jobs+1; i++)
	{
		for (k = 1; k < P.nb_machines+1; k++)
		{
			S.date[i][k] = 0;
		}
	}

	for (i = 1; i < P.nb_machines+1; i++)
	{
		nMachine[i].numero_piece = 0;
		nMachine[i].rang_gamme = 0;
	}

	for (i = 1; i < P.nb_jobs * P.nb_machines + 1; i++)
	{
		j = S.vecteur[i];
		(nJob[j])++;

		// Arc Conjonctifs
		// on ne veut pas que les arcs de 0 aux premières machines soient traitées
		if (nJob[j] > 1)
		{
			date = S.date[j][nJob[j] - 1];
			duree = P.duree_operations[j][nJob[j] - 1];
			if (date + duree > S.date[j][nJob[j]])
			{
				S.date[j][nJob[j]] = date + duree;
				S.pere[j][nJob[j]].numero_piece = j;
				S.pere[j][nJob[j]].rang_gamme = nJob[j] - 1;
			}

		}
		else
		{
			// on met le pere du rang 1 à 0
			S.pere[j][nJob[j]].rang_gamme = 0;
		}

		// Arc Dijonctifs

		machine = P.machines[j][nJob[j]];

			
		if (nMachine[machine+1].rang_gamme != 0) // si le rang vaut 0 -> le tt vaut 0
		{
			nl = nMachine[machine+1].numero_piece;
			nc = nMachine[machine+1].rang_gamme;

			if ((S.date[nl][nc] + P.duree_operations[nl][nc]) > S.date[j][nJob[j]])
			{
				S.date[j][nJob[j]] = (S.date[nl][nc] + P.duree_operations[nl][nc]);

				S.pere[j][nJob[j]].numero_piece = nl;
				S.pere[j][nJob[j]].rang_gamme = nc;

			}
		}
		nMachine[machine+1].numero_piece = j;
		nMachine[machine+1].rang_gamme = nJob[j];

	
	}
	// cout est egal au dernier temps de la piece au plus tot plus le temps que la machine met pour aller de cette piece à *
	// on se trouve sur la dernière machine de la pièce donc -> nb_max_machines

	S.pere[P.nb_jobs+1][P.nb_machines+1].numero_piece = 0;
	S.pere[P.nb_jobs+1][P.nb_machines + 1].rang_gamme = P.nb_machines;
	S.cout = 0;
	// on compare toutes les pieces entre elles pour voir quel chemin prendre
	for (i = 1; i < P.nb_jobs + 1; i++)
	{
		new_cout = S.date[i][P.nb_machines] + P.duree_operations[i][P.nb_machines];
		if (S.cout < new_cout)
		{
			S.cout = new_cout;
			S.pere[P.nb_jobs + 1][P.nb_machines + 1].numero_piece = i;
		}
	}

}

void affiche_pere(t_probleme P, t_solution& S) 
{
	int i, k;
	for (i = 1; i < P.nb_jobs+1; i++)
	{
		for (k = 1; k < P.nb_machines+1; k++)
		{
			cout << i << " " << k << " " << S.pere[i][k].numero_piece << "," << S.pere[i][k].rang_gamme << " " << endl;
		}
		cout << "\n" << endl;
	}
}

/**
 * @brief Effectue une recherche locale pour améliorer une solution.
 *
 * @param P Structure de données du problème.
 * @param S Structure de données de la solution à améliorer (passée par référence).
 * @param nb Nombre d'itérations de recherche locale.
 */
void recherche_locale(t_probleme P, t_solution& S, int nb)
{

	// on va remonter le scénario (on part de *)
	// on compare les solutions et on repart à *
	evaluer_solution(P, S);

	t_solution S2;


	int iter = 0;
	// l'arc allant jusqu'à * ne changera jms donc on part de son pere
	t_couple i = S.pere[P.nb_jobs + 1][P.nb_machines + 1];
	// pere de i
	t_couple j = S.pere[i.numero_piece][i.rang_gamme];

	while ((iter <= nb) && (j.rang_gamme != 0))
	{
		S2 = S;

		// on regarde si i et j ont un arc conjonctif == viennent de la même pièce
		if (j.numero_piece == i.numero_piece)
		{
			i = j;
			j = S.pere[j.numero_piece][j.rang_gamme];
		}
		else {
			// boucle pour chercher lesquelles permuter dans le vecteur
			int indice = 1,
				compteur1 = 0,
				compteur2 = 0,
				ind1 = 0,
				ind2 = 0,
				temp = 0; // indices du vecteur pour permuter 

			while (ind1 == 0)
			{
				if (S.vecteur[indice] == i.numero_piece)
					compteur1++;
				if (compteur1 == i.rang_gamme)
					ind1 = indice;
				indice++;
			}
			indice = 1;

			while (ind2 == 0)
			{
				if (S.vecteur[indice] == j.numero_piece)
					compteur2++;
				if (compteur2 == j.rang_gamme)
					ind2 = indice;
				indice++;
			}
			// permutation
			temp = S2.vecteur[ind1];
			S2.vecteur[ind1] = S2.vecteur[ind2];
			S2.vecteur[ind2] = temp;

			evaluer_solution(P, S2);

			if (S2.cout < S.cout)
			{
				// on a trouvé un graphe de cout inferieur -> le chemin critique a changé
				S = S2;
				// on repart alors tout à droite
				i = S.pere[P.nb_jobs + 1][P.nb_machines + 1];
				j = S.pere[i.numero_piece][i.rang_gamme];
			}
			else // permutation perdante
			{
				// on continue de remonter le chemin
				i = j;
				j = S.pere[j.numero_piece][j.rang_gamme];
			}

		}
		iter++;
	} // on arrête - on a un graphe optimal
	//cout << "Le cout du plus long chemin optimal avec recherche_local est : " << S.cout << endl;
}


/**
 * @brief Effectue une recherche du vecteur optimal.
 *
 * @param P Structure de données du problème.
 * @param S Structure de données de la solution.
 */
void essai_vecteur(t_probleme P, t_solution &S)
{
	int i;
	generer_vecteur(P, S);
	recherche_locale(P, S, 100);

	t_solution S_opt = S;
	for (i = 0; i < 100; i++)
	{
		generer_vecteur(P, S);
		recherche_locale(P, S, 100);
		// on garde le cout optimal 
		if (S.cout < S_opt.cout)
			S_opt = S;
	}	

	cout << "Le cout du plus long chemin optimal est : " << S_opt.cout << endl;
}


/**
 * @brief Génère un voisin aléatoire en inversant deux éléments du vecteur de jobs d'une solution.
 *
 * @param S La solution initiale.
 * @param P Le problème associé.
 * @return Une nouvelle solution voisine.
 */
t_solution genere_voisin(t_solution S, t_probleme P)
{
    t_solution S2 = S;

    // On inverse aléatoirement deux indices du vecteur de jobs
    int index1, index2;
    index1 = 1 + rand() % (P.nb_jobs * P.nb_machines);
    index2 = 1 + rand() % (P.nb_jobs * P.nb_machines);

    // Assurez-vous que les indices sont différents
    while (index1 == index2) {
        index2 = 1 + rand() % (P.nb_jobs * P.nb_machines);
    }

    // Inversez les deux éléments
    int temp = S.vecteur[index1];
    S2.vecteur[index1] = S2.vecteur[index2];
    S2.vecteur[index2] = temp;

    return S2;
}

/**
 * @brief Calcule la valeur de hachage d'une solution.
 *
 * @param P Le problème associé.
 * @param S La solution dont on veut calculer la valeur de hachage.
 * @return La valeur de hachage de la solution.
 */
int f_hash(t_probleme P, t_solution &S)
{
    int somme = 0,
        i, j;
    for (i = 1; i <= P.nb_jobs; i++) {
        for (j = 1; j <= P.nb_machines; j++) {
            somme += S.date[i][j] * S.date[i][j];
        }
    }

    return somme / taille_tab; // On divise par 'taille_tab' pour obtenir la valeur de hachage.
}

/**
 * @brief Implémente l'algorithme GRASP pour résoudre le problème.
 *
 * @param P Le problème à résoudre.
 * @param S La solution initiale (passée par référence).
 * @param nb_rech Le nombre d'itérations de recherche locale.
 * @param nb_cascade Le nombre d'itérations de cascade.
 * @param nb_voisins Le nombre de voisins générés à chaque itération.
 * @return Le coût de la meilleure solution trouvée.
 */
int GRASP(t_probleme P, t_solution &S, int nb_rech, int nb_cascade, int nb_voisins)
{
    int T[taille_tab] = { 0 };

    t_solution S2 = S; // pour ne pas modifier S

    recherche_locale(P, S2, nb_rech);
    t_solution S_opt_voisin = S2;

    T[f_hash(P, S2)] = 1;

    int iter_cascade = 0;
    int iter_voisin = 0;

    while (iter_cascade < nb_cascade)
    {
        // boucle sur les voisins
        while(iter_voisin < nb_voisins)
        {
            t_solution voisin = genere_voisin(S2, P);

            // On cherche un voisin qui n'a pas été utilisé
            if (T[f_hash(P, S2)] == 0) {
                voisin = genere_voisin(S2, P);
                recherche_locale(P, voisin, nb_rech);
            }

            // Mise à jour du tableau
            T[f_hash(P, voisin)] = 1;

            // Comparaison avec la meilleure solution trouvée
            if (voisin.cout < S_opt_voisin.cout)
                S_opt_voisin = voisin;

            iter_voisin++;
        }

        S2 = S_opt_voisin;

        iter_cascade++;
    }

    return S_opt_voisin.cout;
}
