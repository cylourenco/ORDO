#ifndef HEADER_H
#define HEADER_H

#include <iostream>
#include <string>
#include <fstream>
#include <sstream>

const int nb_max_sommet = 210;
const int nb_max_successeur = 9;
const int c_infini = 999;

typedef struct t_graphe
{
	int nb_sommet;
	int successeur[nb_max_sommet][nb_max_successeur];
	int longueur_arc[nb_max_sommet][nb_max_successeur];
	int nb_successeur[nb_max_sommet];

	int marque_sommet[nb_max_sommet];
	int pere[nb_max_sommet];

	int ordre[nb_max_sommet];

}t_graphe;

typedef struct t_T
{

}t_T;

void lire_fichier(t_graphe& g, std::string nom_fichier);   //type standard std::string
void lire_fichier_excel(t_graphe& g, std::string nom_fichier);   //type standard std::string



void calculer_plus_court_chemin_bellman(t_graphe& g, int sommet_depart, int sommet_fin);

void calculer_plus_court_chemin(t_graphe& g, int sommet_d�part, int sommet_final);




#endif

